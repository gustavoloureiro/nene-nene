import 'package:flutter/material.dart';

class Heading extends StatelessWidget {
  Heading({this.deviceHeight, this.title, this.subtitle});

  double deviceHeight;
  String title;
  String subtitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: deviceHeight * 0.12,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Nene nene!",
            style: TextStyle(fontSize: 35, fontWeight: FontWeight.w700),
          ),
          Text(
            "Faça login aqui embaixo",
            style: TextStyle(fontSize: 25, fontWeight: FontWeight.w200),
          ),
        ],
      ),
    );
  }
}